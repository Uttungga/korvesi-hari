import Col from "react-bootstrap/Col";
import { Button, Form, Row, Table } from "react-bootstrap";
import "./App.css";
import { useState, useRef } from "react";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  const inputRef = useRef(null);
  const [day, setDay] = useState(0);
  const [month, setMonth] = useState(0);
  const [year, setYear] = useState(0);

  const konversi = () => {
    let x = inputRef.current.value;
    let y = x % 365;
    let tahun = (x - y) / 365;
    let hari = y % 30;
    let bulan = (y - hari) / 30;

    setDay(hari);
    setMonth(bulan);
    setYear(tahun);
  };

  return (
    <div>
      <Form>
        <Row>
          <h1>Konversi Hari</h1>
          <p>
            <b>1 Tahun 365 hari, 1 Bulan 30 hari</b>
          </p>
        </Row>
        <Row>
          <Col>
            <Form.Control
              type="number"
              ref={inputRef}
              placeholder="Masukkan jumlah hari (angka) ..."
            />
            <Button variant="primary" id="tombol" onClick={konversi}>
              Konversi
            </Button>
          </Col>
          <Col>
            <Table striped bordered>
              <tr>
                <td>
                  <Form.Control
                    type="number"
                    readOnly
                    id="tahun"
                    value={year}
                  />
                </td>
                <td>Tahun</td>
              </tr>
              <tr>
                <td>
                  <Form.Control
                    type="number"
                    readOnly
                    id="bulan"
                    value={month}
                  />
                </td>
                <td>Bulan</td>
              </tr>
              <tr>
                <td>
                  <Form.Control type="number" readOnly id="hari" value={day} />
                </td>
                <td>Hari</td>
              </tr>
            </Table>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

export default App;
